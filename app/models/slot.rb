class Slot
  include Mongoid::Document

  state_machine initial: :off do
    event :fill do
      transition off: :busy
    end
    
    event :empty do
      transition off: :empty
    end
    
    event :err do
      transition any => :disabled
    end
    
    event :release do
      transition busy: :releasing
    end
    
    event :rent do
      transition releasing: :empty
    end
    
    event :cancel_rent do
      transition releasing: :busy
    end
    
    event :return do
      transition empty: :busy
    end

    event :block do
      transition busy: :blocked, releasing: :blocked
    end

    event :unblock do
      transition blocked: :busy
    end
  end

  attr_protected
end
