$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "totem_engine/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "totem_engine"
  s.version     = TotemEngine::VERSION
  s.authors     = ["Stefano Diem Benatti"]
  s.email       = ["stefano@heavenstudio.com.br"]
  s.homepage    = ""
  s.summary     = "A TotemEngine"
  s.description = "A TotemEngine for CompartBike"

  s.files = Dir["{app,config,db,lib}/**/*"] + ["MIT-LICENSE", "Rakefile", "README.rdoc"]

  s.add_dependency "rails", "~> 3.2.7"
  s.add_dependency "state_machine"
  s.add_dependency "mongoid"
end
