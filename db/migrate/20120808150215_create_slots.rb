class CreateSlots < ActiveRecord::Migration
  def change
    create_table :slots do |t|
      t.string :state

      t.timestamps
    end
  end
end
