require 'spec_helper'

describe Slot do
  it { should have_fields(:state).of_type(String) }
  
  describe "states" do
    let(:slot){ Slot.new }
    it "should start as off" do
      slot.should be_off
    end
    
    it "should transit from off to busy" do
      slot.can_fill?.should be_true
      slot.fill
      slot.should be_busy
    end
    
    it "should transit from off to empty" do
      slot.can_empty?.should be_true
      slot.empty
      slot.should be_empty
    end
        
    it "should transit from busy to releasing" do
      slot = Slot.new state: :busy
      slot.can_release?.should be_true
      slot.release
      slot.should be_releasing
    end
    
    it "should transit from releasing to empty" do
      slot = Slot.new state: :releasing
      slot.can_rent?.should be_true
      slot.rent
      slot.should be_empty
    end
    
    it "should transit from releasing back to busy" do
      slot = Slot.new state: :releasing
      slot.can_cancel_rent?.should be_true
      slot.cancel_rent
      slot.should be_busy
    end
    
    it "should transit from empty to busy" do
      slot = Slot.new state: :empty
      slot.can_return?.should be_true
      slot.return
      slot.should be_busy
    end

    it "should transit from busy to blocked" do
      slot = Slot.new state: :busy
      slot.can_block?.should be_true
      slot.block
      slot.should be_blocked
    end

    it "should transit from releasing to blocked" do
      slot = Slot.new state: :releasing
      slot.can_block?.should be_true
      slot.block
      slot.should be_blocked
    end

    it "should transit from blocked to busy" do
      slot = Slot.new state: :blocked
      slot.can_unblock?.should be_true
      slot.unblock
      slot.should be_busy
    end
  end
  
  describe "error" do
    def should_transit_to_err(state)
      slot = Slot.new state: state
      slot.can_err?.should be_true
      slot.err
      slot.should be_disabled
    end
    
    it { should_transit_to_err(:off) }
    it { should_transit_to_err(:empty) }
    it { should_transit_to_err(:busy) }
    it { should_transit_to_err(:releasing) }
  end
end
